# assigns the days of the week as one string
days = "Mon Tue Wed Thu Fri Sat Sun"
# assigns the months as one string but uses '\n' as a separator
months = "\nJan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug\n\n"

# output days
puts "Here are the days: #{days}"
# output months
puts "Here are the months: #{months}"

# output a block quote
puts %q{
There's something going on here.
With this weird quote
We'll be able to type as much as we like.
Even 4 lines if we want, or 5 or 6.
}
