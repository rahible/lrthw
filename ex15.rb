# reads the filename as the first arguement of the application on start
filename = ARGV.first

# opens the file based on the name
txt = open(filename)

# outputs the message
puts "Here's your file #{filename}:"
# prints the contents of the file
print txt.read

txt.close