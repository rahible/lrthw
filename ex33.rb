i = 0
count = 6
incr = 3

def loop_it(index, increment, count) 
  numbers = []
  (index..count).each do |index|
#  while index < count
    puts "At the top i is #{index}"
    numbers << index
    index += increment
    puts "Numbers now: ", numbers
    puts "At the bottom i is #{index}"
  end
  return numbers
end

result = loop_it(i, incr, count)

puts "The numbers: "
result.each do |num|
  puts num
end
