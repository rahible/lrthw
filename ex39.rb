def print_header() 
  puts '-' * 10
end

# create a mapping of state to abrreviation
states = {
  'Oregon' => 'OR',
  'Florida' => 'FL',
  'California' => 'CA',
  'New York' => 'NY', 
  'Michigan' => 'MI'
}

# create a basic set of states and some cities in them
cities = {
  'CA' => 'San Francisco',
  'MI' => 'Detroit',
  'FL' => 'Jacksonville'
}

# add some more cities
cities['NY'] = 'New York'
cities['OR'] = 'Portland'

# puts out some cities
print_header
puts "NY State has: #{cities['NY']}"
puts "OR State has: #{cities['OR']}"

# puts out some states
print_header
puts "Michigan's abbreviation is: #{states['Michigan']}"
puts "Florida's abbreviation is: #{states['Oregon']}"

# puts out every state abbreviation
print_header
states.each do |state, abbrev|
  puts "#{state} is abbreviated #{abbrev}"
end

# puts out every cities
print_header
cities.each do |abbrev, city|
  puts "#{abbrev} has the city #{city}"
end

# now do both at the same time
print_header
states.each do |state, abbrev|
  city = cities[abbrev]
  puts "#{state} is abbreviated #{abbrev} and has city #{city}"
end

print_header
state = states['Texas']

if !state
  puts "Sorry, no Texas."
end

city = cities['TX']
city ||= 'Does Not Exist'
puts "The city for the state 'TX' is: #{city}"

#print_header
#states_sr = states.sort
#states_sorted = states_sr.to_h
#puts states_sorted

#print_header
#cities_sr = cities.sort_by {|k, v| v}
#puts cities_sr
#cities_sorted = cities_sr.to_h
#puts cities_sorted

print_header
puts cities.keys.sort

print_header
puts cities.values.sort