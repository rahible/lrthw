require "./bin/app.rb"
require "test/unit"
require "rack/test"

class GothonWebAppTest < Test::Unit::TestCase
  include Rack::Test::Methods
  
  def app
    Sinatra::Application
  end
  
  test "my_default" do
    get '/'
    assert_equal 'Hello world', last_response.body
  end

  test "hello form" do
    get '/hello/'
    assert last_response.ok?
    assert last_response.body.include?('A Greeting')
  end
  
  test "hello form post" do
    post '/hello/', params={:name => 'Frank', :greeting => "Hi"}
    assert last_response.ok?
    assert last_response.body.include?('I just wanted to say')
  end
end
