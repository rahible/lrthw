# frozen_string_literal: true

require_relative "game/version"
module Game
  class Error < StandardError; end
end
