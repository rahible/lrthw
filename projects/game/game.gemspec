# frozen_string_literal: true

require_relative "lib/game/version"

Gem::Specification.new do |spec|
  spec.name = "game"
  spec.version = Game::VERSION
  spec.authors = ["R. Aaron Hible"]
  spec.email = ["rahible@gmail.com"]

  spec.summary = "Game example for lrthw."
  spec.description = "Game example for lrthw."
  spec.homepage = "http://www.gitlab.com/rahible"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "http://www.gitlab.com/rahible"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "http://www.gitlab.com/rahible"
  spec.metadata["changelog_uri"] = "http://www.gitlab.com/rahible/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
