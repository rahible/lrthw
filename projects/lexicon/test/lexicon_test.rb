# frozen_string_literal: true

require "test_helper"

class LexiconTest < Test::Unit::TestCase
  test "VERSION" do
    assert do
      ::Lexicon.const_defined?(:VERSION)
    end
  end

  test "directions" do
    assert_equal([["direction", "north"]], Lexicon.scan("north"))
    
    result = Lexicon.scan("north south east")
    
    assert_equal([["direction", "north"],
            ["direction", "south"],
            ["direction", "east"]],
            result)
  end
  
  test "verbs" do
     assert_equal([["verb", "go"]], Lexicon.scan("go"))
     
     result = Lexicon.scan("go kill eat")
     
     assert_equal([["verb", "go"],
            ["verb", "kill"],
            ["verb", "eat"]], result)
  end
  
  test "stops" do
    assert_equal([["stop", "the"]], Lexicon.scan("the"))
    
    result = Lexicon.scan("the in of")
    
    assert_equal([["stop", "the"],
            ["stop", "in"],
            ["stop", "of"]], result)
  end
 
  test "nouns" do
    assert_equal([["noun", "bear"]], Lexicon.scan("bear"))
    
    result = Lexicon.scan("bear princess")
    
    assert_equal([["noun", "bear"], ["noun", "princess"]], result)
    
  end

  test "numbers" do
    assert_equal([["number", 1234]], Lexicon.scan("1234"))
    
    result = Lexicon.scan("3 91234")
    
    assert_equal([["number", 3], ["number", 91234]], result)
    
  end 

  test "errors" do
    assert_equal([["error", "ASDFADFASDF"]], Lexicon.scan("ASDFADFASDF"))
    
    result = Lexicon.scan("bear IAS princess")
    
    assert_equal([['noun', 'bear'], ['error', 'IAS'], ['noun', 'princess']], result)
  end

end
