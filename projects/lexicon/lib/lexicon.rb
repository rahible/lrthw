# frozen_string_literal: true

require_relative "lexicon/version"

class Lexicon
  
  @@verbs = ['go', 'stop', 'kill', 'eat']
  @@directions = ['north', 'south', 'east', 'west']
  @@stops = ['the', 'in', 'of']
  @@nouns = ['bear', 'princess']
  
  def self.scan(sentence)
    words = sentence.split(' ')
    result = []
    words.each do |word|
      if @@directions.include? word 
        result << ['direction', word]
      elsif @@verbs.include? word
        result << ['verb', word]
      elsif @@stops.include? word
        result << ['stop', word]
      elsif @@nouns.include? word
        result << ['noun', word]
      elsif !!( word.match(/^(\d)+$/))
        result << ['number', convert_number(word)]
      else
        result << ['error', word]
      end
    end
    return result
  end
  
  def self.convert_number(object)
    begin
      return Integer(object)
    rescue
      return nil
    end
  end
 
end
