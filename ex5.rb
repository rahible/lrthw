name = 'Zed A. Shaw'
age = 35
height_inches = 74
weight_pounds = 180
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'

#convert to centimeters
height = height_inches * 2.54
#convert to kil
weight = weight_pounds * 0.453592


puts "Let's talk about #{name}."
puts "He's #{height} centimeters tall."
puts "He's #{weight} kilograms heavy."
puts "Actually that's not too heavy."
puts "He's got #{eyes} eyes and #{hair} hair."
puts "His teeth are usually #{teeth} depending on the coffee."

puts "If I add #{age}, #{height}, and #{weight} I get #{age + height + weight}."