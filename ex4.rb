# Number of cars in the pool
cars = 100
# Number of people (passengers + driver) can fit in a car
space_in_a_car = 4.0 # converts carpool_capacity to a floating point
drivers = 30
passengers = 90
# If all of the drivers drive a car, the cars left over.
cars_not_driven = cars - drivers
# one driver for each car being driven
cars_driven = drivers
# number of cars driven by the number spaces in the cars
carpool_capacity = cars_driven * space_in_a_car
# passengers (not including drivers) by the number of cars being driven
average_passengers_per_cars = passengers / cars_driven

puts "There are #{cars} cars avaiable."
puts "There are only #{drivers} drivers available."
puts "There will be #{cars_not_driven} empty cars today."
puts "We can transport #{carpool_capacity} people today."
puts "We have #{passengers} to carpool today."
puts "We need to put about #{average_passengers_per_cars} in each car."
