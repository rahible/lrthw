BEGIN {
 puts "Starting the game..."

}

END {
  puts "Thanks for playing!"
}

def outside
  puts "You made it outside, now go home"
  exit(0)
end

def bloody_hands
  puts "Are your hands bloody?"

  while true
    print "> "
    choice = $stdin.gets.chomp
    if choice.upcase.include?("YES")
      puts "wait for police?"
      outside
    elsif choice.upcase.include?("NO")
      puts "call police?"
      outside
    else 
      puts "Make a choice: Yes or No!"
    end
  end

end

def billard_room
  puts "You are in the billard room."
  puts "Do you see a dead body?"
  
  while true
    print "> "
    choice = $stdin.gets.chomp
    
    if choice.upcase.include?("YES")
      bloody_hands
    elsif choice.upcase.include?("NO")
      puts "Play pool?"
      outside
    else
      puts "Make a choice: Yes or No!"
    end
  end
end

def start
  puts "You are facing 2 doors."
  puts "Which do you choose? Right or Left?"
  
  choice = $stdin.gets.chomp
  if choice.upcase.include?("LEFT")
    outside
  elsif choice.upcase.include?("RIGHT")
    billard_room
  else
    puts "Giving up? try again!"
    exit(0)
  end
  
end

start

